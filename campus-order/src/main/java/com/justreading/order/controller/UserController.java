package com.justreading.order.controller;

import java.util.Arrays;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.justreading.order.dao.UserDao;
import com.justreading.order.vo.PersonInfoVo;
import com.justreading.order.vo.UserInfoVo;
import com.justreading.order.vo.UserRegisterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.justreading.order.entity.UserEntity;
import com.justreading.order.service.UserService;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.R;



/**
 * 用户表，用来存放用户
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@RestController
@RequestMapping("order/user")
@Api(tags = "用户接口")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    /**
     * 列表
     */
    @ApiOperation(value = "分页查询用户列表")
    @GetMapping("/list")
//    @RequiresPermissions("order:user:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据用户的id查询用户信息")
    @PostMapping("/info/{id}")
//   @RequiresPermissions("order:user:info")
    public R info(@PathVariable("id") String id){
		UserEntity user = userService.getById(id);

        return R.ok().put("user", user);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "新增用户")
    @PostMapping("/save")
//    @RequiresPermissions("order:user:save")
    public R save(@RequestBody UserEntity user){
		userService.save(user);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation("修改用户信息")
    @PostMapping("/update")
//    @RequiresPermissions("order:user:update")
    public R update(@RequestBody UserEntity user){
		userService.updateById(user);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除用户")
    @PostMapping("/delete")
//    @RequiresPermissions("order:user:delete")
    public R delete(@RequestBody String[] ids){
		userService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    @ApiOperation(value = "通过手机号码获取用户信息")
    @PostMapping("/getUserByPhone")
    public R getUserByPhone(@RequestParam(name = "phone",required = true)String phone){
        userService.getUserByPhone(phone);
//        return R.ok().setData(user);
        return null;
    }

    @ApiOperation(value = "通过用户名获取用户信息")
    @PostMapping("/getUserByUsername")
    public R getUserByUsername(@RequestParam(name = "username",required = true)String username){
        UserEntity user = userDao.selectOne(new QueryWrapper<UserEntity>().eq("username", username));
        return R.ok().setData(user);
    }

    @ApiOperation(value = "注册用户")
    @PostMapping("/register")
    public R registerUser(@RequestBody UserRegisterVo userRegisterVo){
        UserInfoVo userInfoVo = userService.registerUser(userRegisterVo);
        return R.ok().setData(userInfoVo);
    }


    @ApiOperation(value = "通过token来获取用户的信息")
    @PostMapping("/getUserInfoByToken/{token}")
    public R getUserInfoByToken(@PathVariable(name = "token")String token){
        PersonInfoVo personInfoVo = userService.getUserInfoByToken(token);
        return R.ok().setData(personInfoVo);
    }

    @ApiOperation(value = "退出登录")
    @PostMapping("/logout/{token}")
    public R logout(@PathVariable(name = "token")String token){
        userService.logout(token);
        return R.ok();
    }
}
