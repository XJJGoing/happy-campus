package com.justreading.order.controller;

import java.util.Arrays;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.justreading.order.entity.FeedbackEntity;
import com.justreading.order.service.FeedbackService;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.R;



/**
 * 用于反馈投诉用的表
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@RestController
@RequestMapping("order/feedback")
@Api(tags = "反馈接口")
public class FeedbackController {
    @Autowired
    private FeedbackService feedbackService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "分页查询反馈列表信息")
//    @RequiresPermissions("order:feedback:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = feedbackService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @PostMapping("/info/{id}")
    @ApiOperation(value = "根据反馈id查询反馈消息")
//   @RequiresPermissions("order:feedback:info")
    public R info(@PathVariable("id") Long id){
		FeedbackEntity feedback = feedbackService.getById(id);

        return R.ok().put("feedback", feedback);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "添加反馈信息")
    @PostMapping("/save")
//    @RequiresPermissions("order:feedback:save")
    public R save(@RequestBody FeedbackEntity feedback){
		feedbackService.save(feedback);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改反馈信息")
    @PostMapping("/update")
//    @RequiresPermissions("order:feedback:update")
    public R update(@RequestBody FeedbackEntity feedback){
		feedbackService.updateById(feedback);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除反馈信息")
    @PostMapping("/delete")
//    @RequiresPermissions("order:feedback:delete")
    public R delete(@RequestBody Long[] ids){
		feedbackService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
