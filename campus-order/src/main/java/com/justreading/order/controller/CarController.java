package com.justreading.order.controller;

import java.util.Arrays;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.justreading.order.entity.CarEntity;
import com.justreading.order.service.CarService;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.R;



/**
 * 车辆信息表
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@RestController
@RequestMapping("order/car")
@Api(tags = "车辆接口")
public class CarController {
    @Autowired
    private CarService carService;

    /**
     * 列表
     */
    @ApiOperation(value = "分页查询车辆信息")
    @GetMapping("/list")
//    @RequiresPermissions("order:car:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = carService.queryPage(params);
        return R.ok().setData(page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据车辆的id查询车辆信息")
    @PostMapping("/info/{id}")
//   @RequiresPermissions("order:car:info")
    public R info(@PathVariable("id") Long id){
        CarEntity car = carService.getByIdAndDetail(id);
        return R.ok().setData(car);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "增加车辆")
    @PostMapping("/save")
//    @RequiresPermissions("order:car:save")
    public R save(@RequestBody CarEntity car){
		carService.save(car);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation("更新车辆信息")
//    @RequiresPermissions("order:car:update")
    public R update(@RequestBody CarEntity car){
		carService.updateById(car);

        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation(value = "删除车辆")
//    @RequiresPermissions("order:car:delete")
    public R delete(@RequestBody Long[] ids){
		carService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
