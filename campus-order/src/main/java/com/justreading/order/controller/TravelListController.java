package com.justreading.order.controller;

import java.util.Arrays;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.justreading.order.entity.TravelListEntity;
import com.justreading.order.service.TravelListService;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.R;



/**
 * 车主发布的出行单
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@RestController
@RequestMapping("order/travellist")
@Api(tags = "发布的出行单列表接口")
public class TravelListController {
    @Autowired
    private TravelListService travelListService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "分页查询车主发布的出行单列表")
//    @RequiresPermissions("order:travellist:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = travelListService.queryPage(params);
        return R.ok().setData(page);
    }

    @ApiOperation(value = "分页查询所有出行单信息")
    @GetMapping("/getTravelList")
    public R getTravelList(@RequestParam Map<String,Object> params){
        PageUtils page = travelListService.getTravelList(params);
        return R.ok().setData(page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据id查询出行单")
    @PostMapping("/info/{id}")
//   @RequiresPermissions("order:travellist:info")
    public R info(@PathVariable("id") Long id){
		TravelListEntity travelList = travelListService.getById(id);

        return R.ok().setData(travelList);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增出行单")
//    @RequiresPermissions("order:travellist:save")
    public R save(@RequestBody TravelListEntity travelList){
		travelListService.save(travelList);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改出行单")
//    @RequiresPermissions("order:travellist:update")
    public R update(@RequestBody TravelListEntity travelList){
//		travelListService.updateById(travelList);
		travelListService.updateTravelListAndSendMessage(travelList);
        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除出行单")
    @PostMapping("/delete")
//    @RequiresPermissions("order:travellist:delete")
    public R delete(@RequestBody Long[] ids){
		travelListService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
