package com.justreading.order.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.justreading.order.vo.request.TravelOrderListRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.justreading.order.entity.TravelOrderEntity;
import com.justreading.order.service.TravelOrderService;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.R;



/**
 * 用户预约的出行约单
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@RestController
@RequestMapping("order/travelorder")
@Api(tags = "预约单接口")
public class TravelOrderController {
    @Autowired
    private TravelOrderService travelOrderService;

    /**
     * 列表
     */
    @ApiOperation(value = "分页查询约单列表")
    @PostMapping("/list")
//    @RequiresPermissions("order:travelorder:list")
    public R list(@RequestBody TravelOrderListRequest request){
        PageUtils page = travelOrderService.queryPage(request);
        return R.ok().setData(page);
    }

    @ApiOperation(value = "根据用户的id获取用户的约单")
    @PostMapping("/getTravelOrderByUserId/{id}")
    public R getTravelOrderByUserId(@PathVariable(name = "id")Long userId){
        List<TravelOrderEntity> list = travelOrderService.getTravelOrderByUserId(userId);
        return R.ok().setData(list);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据约单的id查询约单列表")
    @PostMapping("/info/{id}")
//   @RequiresPermissions("order:travelorder:info")
    public R info(@PathVariable("id") Long id){
		TravelOrderEntity travelOrder = travelOrderService.getById(id);

        return R.ok().setData(travelOrder);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "预约出行")
    @PostMapping("/save")
//    @RequiresPermissions("order:travelorder:save")
    public R save(@RequestBody TravelOrderEntity travelOrder){
        travelOrderService.saveOrderAndSendMessage(travelOrder);
        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改约单")
    @PostMapping("/update")
//    @RequiresPermissions("order:travelorder:update")
    public R update(@RequestBody TravelOrderEntity travelOrder){
        travelOrderService.updateOrderAndSendMessage(travelOrder);
        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除约单")
    @PostMapping("/delete")
//    @RequiresPermissions("order:travelorder:delete")
    public R delete(@RequestBody Long[] ids){
		travelOrderService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

}
