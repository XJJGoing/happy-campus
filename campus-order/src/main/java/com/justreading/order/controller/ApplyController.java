package com.justreading.order.controller;

import java.util.Arrays;
import java.util.Map;

import com.justreading.order.vo.request.ApplyOwnerListRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.justreading.order.entity.ApplyEntity;
import com.justreading.order.service.ApplyService;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.R;



/**
 * 申请成为车主的申请表
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@RestController
@RequestMapping("order/apply")
@Api(tags = "申请表接口")
public class ApplyController {
    @Autowired
    private ApplyService applyService;

    public ApplyController() {
        super();
    }

    /**
     * 列表
     */
    @ApiOperation(value = "分页查询车主申请表")
    @PostMapping("/list")
//    @RequiresPermissions("order:apply:list")
    public R list(@RequestBody ApplyOwnerListRequest request){
        PageUtils page = applyService.queryPage(request);
        return R.ok().setData(page);
    }



    /**
     * 信息
     */
    @ApiOperation(value = "跟申请表的id查询申请表信息")
    @PostMapping("/info/{id}")
//   @RequiresPermissions("order:apply:info")
    public R info(@PathVariable("id") Long id){
		ApplyEntity apply = applyService.getById(id);

        return R.ok().put("data", apply);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "增加申请表")
    @PostMapping("/save")
//    @RequiresPermissions("order:apply:save")
    public R save(@RequestBody ApplyEntity apply){
//		applyService.save(apply);
        applyService.saveApplyAndSendMessage(apply);
        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "更新申请表")
    @PostMapping("/update")
//    @RequiresPermissions("order:apply:update")
    public R update(@RequestBody ApplyEntity apply){
//		applyService.updateById(apply);
        applyService.updateByIdAndOtherInfo(apply);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation(value = "删除申请表")
//    @RequiresPermissions("order:apply:delete")
    public R delete(@RequestBody Long[] ids){
		applyService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
