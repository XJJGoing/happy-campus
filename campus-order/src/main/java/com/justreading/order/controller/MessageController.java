package com.justreading.order.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.justreading.order.entity.MessageEntity;
import com.justreading.order.service.MessageService;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.R;



/**
 * 用于消息通知
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@RestController
@RequestMapping("order/message")
@Api(tags = "消息接口")
public class MessageController {
    @Autowired
    private MessageService messageService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "分页查询通讯消息列表")
//    @RequiresPermissions("order:message:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = messageService.queryPage(params);
        return R.ok().setData(page);
    }

    @ApiOperation(value = "通过userId分页查询消息")
    @GetMapping("/getByUserId")
    public R getByUserId(@RequestParam Map<String,Object> params){
        PageUtils page = messageService.getByUserId(params);
        return R.ok().setData(page);
    }

    /**
     * 信息
     */
    @PostMapping("/info/{id}")
    @ApiOperation("根据id查询通讯消息")
//   @RequiresPermissions("order:message:info")
    public R info(@PathVariable("id") Long id){
		MessageEntity message = messageService.getById(id);

        return R.ok().put("message", message);
    }

    /**
     * 保存
     */
    @ApiOperation("新增通讯消息")
    @PostMapping("/save")
//    @RequiresPermissions("order:message:save")
    public R save(@RequestBody MessageEntity message){
		messageService.save(message);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改通讯消息")
//    @RequiresPermissions("order:message:update")
    public R update(@RequestBody MessageEntity message){
		messageService.updateById(message);
        return R.ok();
    }

    @PostMapping("/updateBatch")
    @ApiOperation(value = "修改通讯消息")
//    @RequiresPermissions("order:message:update")
    public R update(@RequestBody List<MessageEntity> messages){
        messageService.updateBatchById(messages);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation(value = "删除通讯消息")
//    @RequiresPermissions("order:message:delete")
    public R delete(@RequestBody Long[] ids){
		messageService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
