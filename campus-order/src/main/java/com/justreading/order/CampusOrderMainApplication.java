package com.justreading.order;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 02 月 20 日 15:14
 */
@SpringBootApplication
@EnableTransactionManagement
public class CampusOrderMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(CampusOrderMainApplication.class,args);
    }


//    /**
//     * 将application-mq.yml 扫描进入容器中
//     * @return
//     */
//    @Bean
//    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
//        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
//        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
//        yaml.setResources(new ClassPathResource("application-mq.yml"));
//        configurer.setProperties(yaml.getObject());
//        return configurer;
//    }
}
