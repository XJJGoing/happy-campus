package com.justreading.order.dao;

import com.justreading.order.entity.UserRoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户角色表即这个用户所拥有的角色
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@Mapper
public interface UserRoleDao extends BaseMapper<UserRoleEntity> {
	
}
