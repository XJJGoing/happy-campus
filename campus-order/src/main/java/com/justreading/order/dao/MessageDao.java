package com.justreading.order.dao;

import com.justreading.order.entity.MessageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用于消息通知
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@Mapper
public interface MessageDao extends BaseMapper<MessageEntity> {
	
}
