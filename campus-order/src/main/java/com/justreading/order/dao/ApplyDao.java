package com.justreading.order.dao;

import com.justreading.order.entity.ApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 申请成为车主的申请表
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@Mapper
public interface ApplyDao extends BaseMapper<ApplyEntity> {

    List<ApplyEntity> pageList(@Param("currentPage") Integer currentPage, @Param("limit") Integer limit,@Param("status") Integer status);
}
