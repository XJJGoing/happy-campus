package com.justreading.order.dao;

import com.justreading.order.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表，用来存放用户
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
