package com.justreading.order.dao;

import com.justreading.order.entity.TravelOrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户预约的出行约单
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@Mapper
public interface TravelOrderDao extends BaseMapper<TravelOrderEntity> {

    List<TravelOrderEntity> pageList(@Param("currentPage") Integer currentPage, @Param("limit") Integer limit, @Param("travelOrderUserId") Long travelOrderUserId);
}
