package com.justreading.order.dao;

import com.justreading.order.entity.RoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用于权限控制，分为 管理员、车主、用户三种角色。
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@Mapper
public interface RoleDao extends BaseMapper<RoleEntity> {
	
}
