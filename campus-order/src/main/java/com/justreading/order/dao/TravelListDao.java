package com.justreading.order.dao;

import com.justreading.order.entity.TravelListEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 车主发布的出行单
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
@Mapper
public interface TravelListDao extends BaseMapper<TravelListEntity> {

    List<TravelListEntity> pageList(@Param("page") Integer page, @Param("limit") Integer limit, @Param("userId") Long userId);

}
