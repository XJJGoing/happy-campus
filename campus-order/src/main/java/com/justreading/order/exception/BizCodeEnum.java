package com.justreading.order.exception;

/**
 * @author LYJ
 * @Description 枚举通用的异常
 * @date 2021 年 02 月 23 日 15:40
 */
public enum  BizCodeEnum {

    /**
     * 验证码过期或者错误
     */

    CODE_NOT_MATCH(10001,"验证码过期或者错误"),
    SEATS_NOT_ENOUGH(500,"剩余座位,预约失败");
    private int code;
    private String message;

    BizCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
