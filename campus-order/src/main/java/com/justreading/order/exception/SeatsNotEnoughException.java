package com.justreading.order.exception;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 05 月 18 日 10:08
 */
@Data
public class SeatsNotEnoughException extends RuntimeException implements Serializable {
    private static final long serialVersionUID = 1L;

    private String msg;
    private int code = 500;

    public SeatsNotEnoughException(){

    }

    public SeatsNotEnoughException(String msg){
        super(msg);
        this.msg = msg;
    }

    public SeatsNotEnoughException(String msg,int code){
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}
