package com.justreading.order.exception;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 05 月 10 日 8:53
 */

@Data
public class InvalidateTokenException extends RuntimeException implements Serializable {
    private static final long serialVersionUID = 1L;

    private String msg;
    private int code = 500;

    public InvalidateTokenException(String msg){
        super(msg);
        this.msg = msg;
    }

    public InvalidateTokenException(String msg,int code){
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}
