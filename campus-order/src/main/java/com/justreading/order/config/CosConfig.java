package com.justreading.order.config;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.region.Region;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author LYJ
 * @Description 腾讯云存储
 * @date 2021 年 05 月 09 日 10:51
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.cos")
public class CosConfig {
    private String secretId;
    private String secretKey;
    private String happyCampusBucket;
    private String areaNJ;
    private String pathNJ;


    @Bean
    public COSClient cosClientNJ() {
        COSCredentials cosCredentials = new BasicCOSCredentials(secretId, secretKey);
        Region region = new Region(areaNJ);
        ClientConfig clientConfig = new ClientConfig(region);
        clientConfig.setConnectionTimeout(50 * 1000);
        clientConfig.setSocketTimeout(50 * 1000);
        return new COSClient(cosCredentials, clientConfig);
    }
}
