package com.justreading.order.config;

import com.justreading.order.interceptor.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 05 月 19 日 10:24
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private RequestInterceptor requestInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/order/third/sms/sendCode",
                        "/order/third/cos/uploadFile",
                        "/order/user/register",
                        "/order/third/sms/validCode",
                        "/order/user/getUserByUsername",
                        "/order/user/logout");
    }
}
