package com.justreading.order.config;


import com.justreading.order.utils.SmsComponent;
import com.justreading.order.utils.HttpUtils;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author LYJ
 * @Description 短信消息配置
 * @date 2021 年 02 月 23 日 10:27
 */
@ConfigurationProperties(prefix = "spring.sms")
@Configuration
@Data
public class SmsConfig {


//    @Value("{spring.sms.secretId}")
    private String secretId;

//    @Value("{spring.sms.secretKey}")
    private String secretKey;

//    @Value("{spring.sms.phonePrefix}")
    private String phonePrefix;

//    @Value("${spring.sms.registerTemplate}")
    private String registerTemplate;

//    @Value("${spring.sms.sign}")
    private String sign;

//    @Value("${spring.sms.smsSdkAppid}")
    private String smsSdkAppid;

//    @Value("${spring.sms.expirationTime}")
    private String expirationTime;

    /**
     * 生成smsClient
     * @return
     */
    @Bean
    public SmsClient smsClient(){
        Credential cred = new Credential(secretId, secretKey);
        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint("sms.tencentcloudapi.com");
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        return new SmsClient(cred, "", clientProfile);
    }

    @Bean
    public SmsComponent smsComponent(SmsClient smsClient){
        return new SmsComponent(this,smsClient);
    }

}
