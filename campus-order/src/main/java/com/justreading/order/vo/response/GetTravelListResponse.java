package com.justreading.order.vo.response;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 05 月 17 日 16:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetTravelListResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 出行单id
     */
    private Long id;

    /**
     * 出行时间
     */
    private String travelTime;

    /**
     * 出行目的
     */
    private String travelPoint;

    /**
     * 出行目的经度
     */
    private String travelPointY;

    /**
     * 出行目的维度
     */
    private String travelPointX;

    /**
     * 上车时间
     */
    private String boardingTime;

    /**
     * 上车地点
     */
    private String boardingPoint;

    /**
     * 上车地点经度
     */
    private String boardingPointY;

    /**
     * 上车地点维度
     */
    private String boardingPointX;

    /**
     * 座位
     */
    private Integer seats;

    /**
     * 价格/位
     */
    private BigDecimal price;

    /**
     * 发布出行单用户Id
     */
    private Long userId;

    /**
     * 出行车辆Id
     */
    private Long carId;

    /**
     * 出行单状态【0 新建 1 已出发 2 已完成  3 已过期】
     */
    private Integer status;
    /**
     * 出行单备注信息
     */
    private String note;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 更新时间
     */
    private String updateTime;


    /**
     * 发单人真实姓名
     */
    private String trueName;

    /**
     * 发单人手机号码
     */
    private String mobile;

    /**
     * 发单人头像
     */
    private String header;

    /**
     * 发单人性别
     */
    private Integer gender;

    /**
     * 发单人学校
     */
    private String school;

    /**
     * 发单人学院
     */
    private String college;

    /**
     * 发单人学号//工号
     */
    private String jobId;

    /**
     * 发布量
     */
    private Integer releaseNumber;

    /**
     * 成长值
     */
    private Integer growth;

    /**
     * 积分
     */
    private Integer integration;

    /**
     * 履约率
     */
    private Double complianceRate;

    /**
     * 车牌号
     */
    private String licensePlate;

    /**
     * 车辆图片
     */
    private List<String> imageList;

    /**
     * 车辆说明
     */
    private String description;

    /**
     * 车辆颜色
     */
    private String color;
}
