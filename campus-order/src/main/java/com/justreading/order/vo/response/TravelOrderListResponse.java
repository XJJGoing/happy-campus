package com.justreading.order.vo.response;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 05 月 11 日 10:46
 */
@Data
public class TravelOrderListResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 预约单
     */
    private Long id;

    /**
     * 出行单Id
     */
    private Long travelListId;
    /**
     * 出行人Id
     */
    private Long travelOrderUserId;
    /**
     * 出行人抵达状态【0 未抵达 1 抵达 】
     */
    private Integer arriveStatus;
    /**
     * 出行人姓名
     */
    private String travelOrderUserTrueName;
    /**
     * 出行人电话
     */
    private String travelOrderUserMobile;

    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 出行order状态【0 预订  1 出行中  2 已抵达  3 出行单已取消  4 取消Order直接删除】
     */
    private Integer orderStatus;
    /**
     * 发单人Id
     */
    private Long travelListUserId;
    /**
     * 发单人姓名
     */
    private String travelListUserTrueName;
    /**
     * 发单人电话
     */
    private String travelListUserMobile;

    /**
     * 出发地点
     */
    private String travelPoint;

    /**
     * 到达地点
     */
    private String boardingPoint;

    /**
     * 车牌号
     */
    private String licensePlate;

    /**
     * 出行时间
     */
    private String travelTime;
}
