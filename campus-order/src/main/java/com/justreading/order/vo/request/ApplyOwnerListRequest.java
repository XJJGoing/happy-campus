package com.justreading.order.vo.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 05 月 12 日 12:56
 */
@Data
public class ApplyOwnerListRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    private String page;

    /**
     * 每页显示的个数
     */
    private String limit;

    /**
     * 审核状态
     */
    private Integer status;
}
