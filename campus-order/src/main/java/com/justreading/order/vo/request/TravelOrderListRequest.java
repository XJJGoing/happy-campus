package com.justreading.order.vo.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LYJ
 * @Description 请求
 * @date 2021 年 05 月 11 日 10:43
 */
@Data
public class TravelOrderListRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    private String page;

    /**
     * 每页显示的个数
     */
    private String limit;

    /*
     *  出行人的id
     */
    private Long travelOrderUserId;
}
