package com.justreading.order.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 02 月 23 日 23:34
 */
@Data
public class UserInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * userId
     */
    private Long id;

    /**
     * 昵称
     */
    private String nickName;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 头像
     */
    private String header;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 生日
     */
    private Date birth;
    /**
     * 所在城市
     */
    private String city;
    /**
     * 学校
     */
    private String school;
    /**
     * 学院
     */
    private String college;
    /**
     * 在校工号、学号【老师 则 可填工号】
     */
    private String jobId;
    /**
     * 真实姓名
     */
    private String trueName;
    /**
     * 积分
     */
    private Integer integration;
    /**
     * 成长值
     */
    private Integer growth;

    /**
     * 登录token
     */
    private  String token;

    /**
     * 是否为车主
     */
    private Integer isCarOwner;
}
