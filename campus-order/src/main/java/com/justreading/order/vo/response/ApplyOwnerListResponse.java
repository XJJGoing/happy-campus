package com.justreading.order.vo.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 05 月 12 日 12:52
 */
@Data
public class ApplyOwnerListResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;
    /**
     * 申请的用户id
     */
    private Long applyUserId;
    /**
     * 身份证号码
     */
    private String idCard;
    /**
     * 身份证正面照
     */
    private String idCardFrontImage;
    /**
     * 身份证反面照
     */
    private String idCardBackImage;
    /**
     * 驾驶证照片
     */
    private String driverLicenseImage;
    /**
     * 车牌号
     */
    private String licensePlate;
    /**
     * 车辆颜色
     */
    private String color;
    /**
     * 车辆描述信息
     */
    private String description;
    /**
     * 车辆的图片【字符串数组使用,分割】
     */
    private String carImages;
    /**
     * 申请状态【0  未审批 1 申请通过  2 申请未通过】
     */
    private Integer status;
    /**
     * 审批用户的id
     */
    private Long approvalUserId;
    /**
     * 审批用户的姓名
     */
    private String approvalUserTrueName;
    /**
     * 不通过的原因
     */
    private String noPassReason;


    /**
     * 真实姓名
     */
    private String trueName;

    /**
     * 学校
     */
    private String school;
    /**
     * 学院
     */
    private String college;

    /**
     * 在校工号、学号【老师 则 可填工号】
     */
    private String jobId;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 创建时间
     */
    private String createTime;
}
