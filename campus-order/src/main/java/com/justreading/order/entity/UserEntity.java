package com.justreading.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户表，用来存放用户
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-05-09 09:42:47
 */
@Data
@TableName("sys_user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 昵称
	 */
	private String nickName;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 头像
	 */
	private String header;
	/**
	 * 性别
	 */
	private Integer gender;
	/**
	 * 生日
	 */
	private Date birth;
	/**
	 * 所在城市
	 */
	private String city;
	/**
	 * 学校
	 */
	private String school;
	/**
	 * 学院
	 */
	private String college;
	/**
	 * 在校工号、学号【老师 则 可填工号】
	 */
	private String jobId;
	/**
	 * 真实姓名
	 */
	private String trueName;
	/**
	 * 锁定用户【0正常,1锁定】
	 */
	private Integer isLock;

	/**
	 * 积分
	 */
	private Integer integration;
	/**
	 * 成长值
	 */
	private Integer growth;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 发布量
	 */
	private Integer releaseNumber;
	/**
	 * 履约率
	 */
	private Double complianceRate;

	/**
	 * 是否为车主
	 */
	private Integer isCarOwner;

}
