package com.justreading.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用于反馈投诉用的表
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-05-09 09:42:47
 */
@Data
@TableName("sys_feedback")
public class FeedbackEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 反馈人的姓名
	 */
	private String userTrueName;
	/**
	 * 反馈人的id
	 */
	private Long userId;
	/**
	 * 反馈内容
	 */
	private String feedbackContent;
	/**
	 * 反馈图片一个字符串数组
	 */
	private String feedbackImages;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 反馈处理状态【0 未处理 1 已处理】
	 */
	private Integer status;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 投诉单号id
	 */
	private Long travelId;

}
