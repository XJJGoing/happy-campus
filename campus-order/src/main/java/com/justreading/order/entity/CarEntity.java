package com.justreading.order.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 车辆信息表
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-05-09 09:42:47
 */
@Data
@TableName("sys_car")
public class CarEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 车牌号
	 */
	private String licensePlate;
	/**
	 * 车辆颜色
	 */
	private String color;
	/**
	 * 车辆描述信息
	 */
	private String description;
	/**
	 * 车辆图片【字符串数组使用,分割】
	 */
	private String images;
	/**
	 * 车辆的车主ID
	 */
	private Long userId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 保险类型
	 */
	private String insuranceType;
	/**
	 * 保险状态
	 */
	private String insuranceStatus;
	/**
	 * 保险额度
	 */
	private String insuranceLimit;

	/**
	 * 车辆图片列表
	 */
	@TableField(exist = false)
	private List<String> imageList;

}
