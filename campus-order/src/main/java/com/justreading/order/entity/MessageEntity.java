package com.justreading.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 用于消息通知
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-05-09 09:42:47
 */
@Data
@TableName("sys_message")
public class MessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 消息的接收者
	 */
	private Long receiveUserId;
	/**
	 * 消息的发送者
	 */
	private Long sendUserId;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * 是否已读【0 未读 1已读】
	 */
	private Integer isRead;

	/**
	 * 更新时间
	 */
	private String updateTime;

}
