package com.justreading.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 申请成为车主的申请表
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-05-09 09:42:47
 */
@Data
@TableName("sys_apply")
public class ApplyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 申请的用户id
	 */
	private Long applyUserId;
	/**
	 * 身份证号码
	 */
	private String idCard;
	/**
	 * 身份证正面照
	 */
	private String idCardFrontImage;
	/**
	 * 身份证反面照
	 */
	private String idCardBackImage;
	/**
	 * 驾驶证照片
	 */
	private String driverLicenseImage;
	/**
	 * 车牌号
	 */
	private String licensePlate;
	/**
	 * 车辆颜色
	 */
	private String color;
	/**
	 * 车辆描述信息
	 */
	private String description;
	/**
	 * 车辆的图片【字符串数组使用,分割】
	 */
	private String carImages;
	/**
	 * 申请状态【0  未审批 1 申请通过  2 申请未通过】
	 */
	private Integer status;
	/**
	 * 审批用户的id
	 */
	private Long approvalUserId;
	/**
	 * 审批用户的姓名
	 */
	private String approvalUserTrueName;
	/**
	 * 不通过的原因
	 */
	private String noPassReason;
	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * 更新时间
	 */
	private String updateTime;

}
