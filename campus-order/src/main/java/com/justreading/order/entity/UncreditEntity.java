package com.justreading.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-05-09 09:42:47
 */
@Data
@TableName("sys_uncredit")
public class UncreditEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 违约记录表id
	 */
	@TableId
	private Long id;
	/**
	 * 违约用户id
	 */
	private Long userId;
	/**
	 * 违约内容
	 */
	private String uncreditNote;
	/**
	 * 违约时间
	 */
	private Date uncreditTime;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
