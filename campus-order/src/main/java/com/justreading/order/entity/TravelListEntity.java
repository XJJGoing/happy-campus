package com.justreading.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 车主发布的出行单
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-05-09 09:42:47
 */
@Data
@TableName("sys_travel_list")
public class TravelListEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 出行时间
	 */
	private String travelTime;
	/**
	 * 出行目的
	 */
	private String travelPoint;
	/**
	 * 出行目的经度
	 */
	private String travelPointY;
	/**
	 * 出行目的维度
	 */
	private String travelPointX;
	/**
	 * 上车时间
	 */
	private String boardingTime;
	/**
	 * 上车地点
	 */
	private String boardingPoint;
	/**
	 * 上车地点经度
	 */
	private String boardingPointY;
	/**
	 * 上车地点维度
	 */
	private String boardingPointX;
	/**
	 * 座位
	 */
	private Integer seats;
	/**
	 * 价格/位
	 */
	private BigDecimal price;
	/**
	 * 发布出行单用户Id
	 */
	private Long userId;
	/**
	 * 出行车辆Id
	 */
	private Long carId;
	/**
	 * 出行单状态【0 新建 1 已出发 2 已完成  3 已过期】
	 */
	private Integer status;
	/**
	 * 出行单备注信息
	 */
	private String note;
	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * 更新时间
	 */
	private String updateTime;

}
