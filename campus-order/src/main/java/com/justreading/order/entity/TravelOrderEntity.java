package com.justreading.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户预约的出行约单
 * 
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-05-09 09:42:47
 */
@Data
@TableName("sys_travel_order")
public class TravelOrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 出行单Id
	 */
	private Long travelListId;
	/**
	 * 出行人Id
	 */
	private Long travelOrderUserId;
	/**
	 * 出行人抵达状态【0 未抵达 1 抵达 】
	 */
	private Integer arriveStatus;
	/**
	 * 出行人姓名
	 */
	private String travelOrderUserTrueName;
	/**
	 * 出行人电话
	 */
	private String travelOrderUserMobile;

	/**
	 * 创建时间
	 */
	private String createTime;
	/**
	 * 更新时间
	 */
	private String updateTime;
	/**
	 * 出行order状态【0 预订  1 出行中  2 已抵达  3 出行单已取消  4 取消Order直接删除】
	 */
	private Integer orderStatus;
	/**
	 * 发单人Id
	 */
	private Long travelListUserId;
	/**
	 * 发单人姓名
	 */
	private String travelListUserTrueName;
	/**
	 * 发单人电话
	 */
	private String travelListUserMobile;

}
