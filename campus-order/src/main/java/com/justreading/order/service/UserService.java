package com.justreading.order.service;

import cn.hutool.system.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.justreading.common.utils.PageUtils;
import com.justreading.order.entity.UserEntity;
import com.justreading.order.vo.PersonInfoVo;
import com.justreading.order.vo.UserInfoVo;
import com.justreading.order.vo.UserRegisterVo;

import java.util.Map;

/**
 * 用户表，用来存放用户
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 检查数据库是否存在这个用户，存在直接算登录，返回用户的token和基本的用户信息
     * @param phone
     * @return
     */
    UserInfoVo getUserByPhone(String phone);

    UserInfoVo registerUser(UserRegisterVo userRegisterVo);

    PersonInfoVo getUserInfoByToken(String token);

    void logout(String token);
}

