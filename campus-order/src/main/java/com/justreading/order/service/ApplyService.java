package com.justreading.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.justreading.common.utils.PageUtils;
import com.justreading.order.entity.ApplyEntity;
import com.justreading.order.vo.request.ApplyOwnerListRequest;

import java.util.Map;

/**
 * 申请成为车主的申请表
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
public interface ApplyService extends IService<ApplyEntity> {

    PageUtils queryPage(ApplyOwnerListRequest request);

    void saveApplyAndSendMessage(ApplyEntity apply);

    void updateByIdAndOtherInfo(ApplyEntity apply);

}

