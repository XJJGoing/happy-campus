package com.justreading.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.justreading.common.utils.PageUtils;
import com.justreading.order.entity.TravelListEntity;

import java.util.Map;

/**
 * 车主发布的出行单
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
public interface TravelListService extends IService<TravelListEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getTravelList(Map<String, Object> params);

    void updateTravelListAndSendMessage(TravelListEntity travelList);
}

