package com.justreading.order.service.impl;

import com.justreading.order.constant.CarImagesSeparatorConstant;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.Query;

import com.justreading.order.dao.CarDao;
import com.justreading.order.entity.CarEntity;
import com.justreading.order.service.CarService;


@Service("carService")
public class CarServiceImpl extends ServiceImpl<CarDao, CarEntity> implements CarService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        long userId = Long.parseLong(params.get("userId").toString());
        IPage<CarEntity> page = this.page(
                new Query<CarEntity>().getPage(params),
                new QueryWrapper<CarEntity>().eq("user_id",userId)
        );
        List<CarEntity> records = page.getRecords();
        List<CarEntity> newRecords = records.stream().peek((entity) -> {
            String images = entity.getImages();
            String[] split = images.split(CarImagesSeparatorConstant.CAR_SEPARATOR_1);
            List<String> imageList = Arrays.asList(split);
            entity.setImageList(imageList);
        }).collect(Collectors.toList());
        page.setRecords(newRecords);
        return new PageUtils(page);
    }

    @Override
    public CarEntity getByIdAndDetail(Long id) {
        CarEntity carEntity = this.getById(id);
        String images = carEntity.getImages();
        String[] imgSplit = images.split(CarImagesSeparatorConstant.CAR_SEPARATOR_1);
        List<String> imageList = new ArrayList<>(Arrays.asList(imgSplit));
        carEntity.setImageList(imageList);
        return carEntity;
    }

}