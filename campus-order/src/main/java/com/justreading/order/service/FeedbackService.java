package com.justreading.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.justreading.common.utils.PageUtils;
import com.justreading.order.entity.FeedbackEntity;

import java.util.Map;

/**
 * 用于反馈投诉用的表
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
public interface FeedbackService extends IService<FeedbackEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

