package com.justreading.order.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.justreading.order.constant.CarImagesSeparatorConstant;
import com.justreading.order.constant.MessageConstant;
import com.justreading.order.entity.*;
import com.justreading.order.service.CarService;
import com.justreading.order.service.MessageService;
import com.justreading.order.service.UserService;
import com.justreading.order.vo.request.ApplyOwnerListRequest;
import com.justreading.order.vo.response.ApplyOwnerListResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.Query;

import com.justreading.order.dao.ApplyDao;
import com.justreading.order.service.ApplyService;
import org.springframework.transaction.annotation.Transactional;


@Service("applyService")
public class ApplyServiceImpl extends ServiceImpl<ApplyDao, ApplyEntity> implements ApplyService {

    @Autowired
    private MessageService messageService;

    @Autowired
    private UserService userService;

    @Autowired
    private CarService carService;

    @Override
    public PageUtils queryPage(ApplyOwnerListRequest request) {
        String currentPage = request.getPage();
        String limit = request.getLimit();
        Integer status = request.getStatus();
        int page = Integer.parseInt(currentPage);
        if(page != 0){
            page = Integer.parseInt(limit) * page;
        }
        List<ApplyEntity> applyEntities = this.baseMapper.pageList(page, Integer.parseInt(limit), status);
        List<ApplyOwnerListResponse> responseList = applyEntities.stream().map((entity) -> {
            ApplyOwnerListResponse response = new ApplyOwnerListResponse();
            BeanUtils.copyProperties(entity, response);
            UserEntity userEntity = userService.getById(entity.getApplyUserId());
            response.setTrueName(userEntity.getTrueName());
            response.setSchool(userEntity.getSchool());
            response.setCollege(userEntity.getCollege());
            response.setJobId(userEntity.getJobId());
            return response;
        }).collect(Collectors.toList());
        Page<ApplyOwnerListResponse> responsePage = new Page<>(Long.parseLong(currentPage),Long.parseLong(limit));
        responsePage.setRecords(responseList);
        return new PageUtils(responsePage);
    }

    @Transactional
    @Override
    public void saveApplyAndSendMessage(ApplyEntity apply) {
        this.save(apply);
        MessageEntity messageEntity = new MessageEntity();
        messageEntity.setContent(MessageConstant.APPLY_OWNER_SUBMIT_SUCCESS);
        messageEntity.setReceiveUserId(apply.getApplyUserId());
        messageService.save(messageEntity);
    }

    @Transactional
    @Override
    public void updateByIdAndOtherInfo(ApplyEntity apply) {
        //1、更新申请表状态
        Integer status = apply.getStatus();
        Long applyUserId = apply.getApplyUserId();
        this.updateById(apply);
        if(status.equals(1)){

            //2、改变sys_user中的状态为车主
            UserEntity userEntity = new UserEntity();
            userEntity.setId(applyUserId);
            userEntity.setIsCarOwner(1);
            userService.updateById(userEntity);

            //3、将车辆的信息添加到车辆表中
            CarEntity carEntity = new CarEntity();
            carEntity.setColor(apply.getColor());
            carEntity.setDescription(apply.getDescription());
            carEntity.setLicensePlate(apply.getLicensePlate());
            carEntity.setImages(apply.getCarImages() + CarImagesSeparatorConstant.CAR_SEPARATOR_1);
            carEntity.setUserId(applyUserId);
            carService.save(carEntity);

            //4、发送申请通过的消息
            MessageEntity messageEntity = new MessageEntity();
            messageEntity.setContent(MessageConstant.APPLY_OWNER_SUCCESS);
            messageEntity.setReceiveUserId(applyUserId);
            messageService.save(messageEntity);
        }else if(status.equals(2)){  //发送审核不通过的消息
            MessageEntity messageEntity = new MessageEntity();
            messageEntity.setContent(MessageConstant.APPLY_OWNER_FAIL + apply.getNoPassReason());
            messageEntity.setReceiveUserId(applyUserId);
            messageService.save(messageEntity);
        }
    }

}