package com.justreading.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.justreading.common.utils.PageUtils;
import com.justreading.order.entity.CarEntity;

import java.util.Map;

/**
 * 车辆信息表
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
public interface CarService extends IService<CarEntity> {

    PageUtils queryPage(Map<String, Object> params);

    CarEntity getByIdAndDetail(Long id);
}

