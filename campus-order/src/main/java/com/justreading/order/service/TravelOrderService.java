package com.justreading.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.justreading.common.utils.PageUtils;
import com.justreading.order.entity.TravelOrderEntity;
import com.justreading.order.vo.request.TravelOrderListRequest;

import java.util.List;
import java.util.Map;

/**
 * 用户预约的出行约单
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
public interface TravelOrderService extends IService<TravelOrderEntity> {

    PageUtils queryPage(TravelOrderListRequest request);

    List<TravelOrderEntity> getTravelOrderByUserId(Long userId);

    void saveOrderAndSendMessage(TravelOrderEntity travelOrder);

    void updateOrderAndSendMessage(TravelOrderEntity travelOrderEntity);

}

