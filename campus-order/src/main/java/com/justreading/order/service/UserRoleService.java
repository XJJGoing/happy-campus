package com.justreading.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.justreading.common.utils.PageUtils;
import com.justreading.order.entity.UserRoleEntity;

import java.util.Map;

/**
 * 用户角色表即这个用户所拥有的角色
 *
 * @author lyj
 * @email 1499755237@qq.com
 * @date 2021-02-20 14:23:35
 */
public interface UserRoleService extends IService<UserRoleEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

