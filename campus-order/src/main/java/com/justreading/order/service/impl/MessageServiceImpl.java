package com.justreading.order.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.Query;

import com.justreading.order.dao.MessageDao;
import com.justreading.order.entity.MessageEntity;
import com.justreading.order.service.MessageService;


@Service("messageService")
public class MessageServiceImpl extends ServiceImpl<MessageDao, MessageEntity> implements MessageService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MessageEntity> page = this.page(
                new Query<MessageEntity>().getPage(params),
                new QueryWrapper<MessageEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getByUserId(Map<String, Object> params) {
        long userId = Long.parseLong(params.get("userId").toString());
        IPage<MessageEntity> page = this.page(
                new Query<MessageEntity>().getPage(params),
                new QueryWrapper<MessageEntity>().eq("receive_user_id", userId).orderByDesc("create_time")
        );
        return new PageUtils(page);
    }

}