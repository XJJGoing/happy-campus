package com.justreading.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.justreading.order.constant.TokenConstant;
import com.justreading.order.entity.UserEntity;
import com.justreading.order.utils.RedisUtil;
import com.justreading.order.utils.TokenManager;
import com.justreading.order.vo.PersonInfoVo;
import com.justreading.order.vo.UserInfoVo;
import com.justreading.order.vo.UserRegisterVo;
import io.swagger.models.auth.In;
import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.justreading.common.utils.PageUtils;
import com.justreading.common.utils.Query;

import com.justreading.order.dao.UserDao;
import com.justreading.order.service.UserService;
import org.springframework.util.ObjectUtils;


@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private TokenManager tokenManager;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserEntity> page = this.page(
                new Query<UserEntity>().getPage(params),
                new QueryWrapper<UserEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public UserInfoVo getUserByPhone(String phone) {
        UserEntity userEntity = this.baseMapper.selectOne(new QueryWrapper<UserEntity>().eq("mobile", phone));
//        if(!ObjectUtils.isEmpty(userEntity)){
//            UserInfoVo userInfoVo = new UserInfoVo();
//            BeanUtils.copyProperties(userEntity,userInfoVo);
//            String token = TokenManager.createToken(userEntity.getUsername());
//        }
        return null;
    }

    @Override
    public UserInfoVo registerUser(UserRegisterVo userRegisterVo) {
        UserEntity userEntity = new UserEntity();
        UserInfoVo userInfoVo = new UserInfoVo();
        BeanUtils.copyProperties(userRegisterVo,userEntity);
        this.baseMapper.insert(userEntity);
        BeanUtils.copyProperties(userEntity,userInfoVo);
        String token = tokenManager.createToken(userEntity.getId().toString());
        redisUtil.set(TokenConstant.TOKEN_USER_ENTITY_PRE + token,userEntity.getId());
        userInfoVo.setToken(token);
        return userInfoVo;
    }

    @Override
    public PersonInfoVo getUserInfoByToken(String token) {
        Integer userId = (Integer)redisUtil.get(TokenConstant.TOKEN_USER_ENTITY_PRE + token);
        UserEntity userEntity = this.getById(userId);
        PersonInfoVo personInfoVo = new PersonInfoVo();
        BeanUtils.copyProperties(userEntity,personInfoVo);
        return personInfoVo;
    }

    @Override
    public void logout(String token) {
        redisUtil.del(token);
    }
}