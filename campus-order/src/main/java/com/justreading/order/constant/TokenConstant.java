package com.justreading.order.constant;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 02 月 26 日 22:22
 */
public class TokenConstant {
    public static final String TOKEN_USER_ENTITY_PRE = "token:userId:";
}
