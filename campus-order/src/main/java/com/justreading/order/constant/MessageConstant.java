package com.justreading.order.constant;

/**
 * @author LYJ
 * @Description 发送消息的通用模板
 * @date 2021 年 05 月 12 日 10:33
 */
public interface MessageConstant {

    String APPLY_OWNER_SUBMIT_SUCCESS = "成功提交车主申请表";

    String APPLY_OWNER_SUBMIT_FAIL = "失败提交车主申请表";

    String APPLY_OWNER_SUCCESS = "！恭喜您，光荣的成为一名车主";

    String APPLY_OWNER_FAIL = "！很遗憾，您的车主申请资料不符合平台要求，请更改后重新提交。原因:";

    String MESSAGE_PLAT_SEND_USER = "平台审核人员";

    String ORDER_OWNER_SUCCESS = " 已预约您的出行单，请您根据电话联系对方或按时到达出发地点";

    String ORDER_USER_SUCCESS = "成功预约:";

    String ORDER_OWNER_CANCEL = "已取消出行";

    String ORDER_USER_CANCEL = "成功取消";


    ///更改travelList状态发送的消息
    String ORDER_UPDATE = "车主修改了出行单信息,请前往预约记录查看";

    String ORDER_START = "已出发";

    String ORDER_END = "已到达";

    String ORDER_CANCEL = "已被车主取消";

    String ORDER_EXPIRATION = "已过期";

}
