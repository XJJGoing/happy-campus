package com.justreading.order.constant;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 02 月 23 日 15:19
 */
public class SmsConstant {
    public static final String SMS_PHONE_PRE = "sms:phone:";
}
