package com.justreading.order.utils;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.justreading.order.config.SmsConfig;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import com.tencentcloudapi.sms.v20190711.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author LYJ
 * @Description
 * @date 2021 年 02 月 23 日 15:01
 */
@Slf4j
public class SmsComponent {

    private Logger logger = LoggerFactory.getLogger(SmsComponent.class);

    private SmsConfig smsConfig;

    private SmsClient smsClient;

    public SmsComponent(SmsConfig smsConfig,SmsClient smsClient) {
        this.smsConfig = smsConfig;
        this.smsClient = smsClient;
    }

    /**
     * 发送短信验证码
     *
     * @param code  验证码
     * @param phone 手机号码
     */
    public void sendCode(String phone, String code) {
        try {
            SendSmsRequest req = new SendSmsRequest();
            String[] phoneNumberSet1 = {smsConfig.getPhonePrefix() + phone};
            req.setPhoneNumberSet(phoneNumberSet1);
            req.setTemplateID(smsConfig.getRegisterTemplate());
            req.setSign(smsConfig.getSign());
            String[] templateParamSet1 = {code, smsConfig.getExpirationTime()};
            req.setTemplateParamSet(templateParamSet1);
            req.setSmsSdkAppid(smsConfig.getSmsSdkAppid());
            SendSmsResponse resp = smsClient.SendSms(req);
        } catch (TencentCloudSDKException e) {
            logger.info("{}手机号码发送验证码失败,失败原因------->{}",phone,e.getMessage());
        }
    }
}
