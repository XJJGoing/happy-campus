package com.justreading.order.third.oss;

import com.justreading.common.utils.R;
import com.justreading.order.utils.CosUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 05 月 09 日 17:10
 */
@Api(tags = "上传文件")
@RestController
@RequestMapping("order/third/cos")
public class CosController {

    @Autowired
    private CosUtil cosUtil;

    @ApiOperation("上传文件并且返回地址")
    @PostMapping("/uploadFile")
    public R uploadFileAndReturnPath(@RequestParam(name = "file",required = true) MultipartFile file){
        String path = cosUtil.addResourceIntoBucketUploadFile(file);
        return R.ok().setData(path);
    }
}
