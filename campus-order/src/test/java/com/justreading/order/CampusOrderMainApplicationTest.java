package com.justreading.order;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 02 月 20 日 19:40
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class CampusOrderMainApplicationTest {

    @Autowired
    private DataSource dataSource;

    @Test
    public void contextLoader(){
        System.out.println(dataSource);
    }
}
