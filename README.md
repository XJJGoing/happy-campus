# happy-campus

#### 介绍
校园乐行APP毕业设计

#### 软件架构
使用SpringCloud的GateWay + mybatisPlus + springBoot + COS + SMS 基础SDK
#### 安装教程

1.  使用Git clone克隆项目
2.  下载idea
3.  导入模块

#### 使用说明

1.  整体功能模块分为公共模块、第三方模块、生成器模块、核心拼车模块、网关模块
2.  使用maven package 将各个模块打包成jar包
3.  在含有JDK环境的服务器中执行 nohup java -jar xxx.jar & 开启服务

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
