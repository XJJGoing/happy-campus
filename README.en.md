# happy-campus

#### Description
校园乐行后端

#### Software Architecture
Use gateway + mybatisPlus + springboot + cos + SMS basic SDK of spring cloud

#### Installation

1. Clone the project with git clone

2. Download idea

3. Import module

#### Instructions

1. The whole function module is divided into public module, third-party module, generator module, core carpool module and gateway module

2. Use Maven package to package each module into jar package

3. Execute nohup Java - jar XXX. Jar & open service in the server with JDK environment

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
