package com.justreading.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author LYJ
 * @Description
 * @date 2021 年 02 月 20 日 19:13
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class CampusGateWayMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(CampusGateWayMainApplication.class, args);
    }
}
